import { defineStore } from "pinia";

export const useAssigmentTypeList = defineStore("assigmentTypeList", {
  state: () => ({
    assigmentTypeList: [],
  }),
});
