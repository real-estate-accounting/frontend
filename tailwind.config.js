/** @type {import('tailwindcss').Config} */
// eslint-disable-next-line no-undef
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        "accent-color": "#7568C0",
        "bg-color": "#EEEEF4",
        "card-color": "#FEFEFE",
      },
      fontFamily: {
        inter: "Inter",
      },
    },
  },
  plugins: [],
};
