import { createRouter, createWebHistory } from "vue-router";
import EstateObjectList from "../views/EstateObjectList.vue";
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "EstateObjectList",
      component: EstateObjectList,
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../views/LogIn.vue"),
    },
    {
      path: "/estate-objects/:id/",
      name: "estate-object-detail",
      component: () => import("../views/EstateObjectDetail.vue"),
    },
  ],
});

import { login, refresh } from "../api/users.js";
import { useUserList } from "../stores/userList";
router.beforeEach(async (to, from, next) => {
  const userList = useUserList();

  if (userList.token) {
    await refresh(userList.token);
  } else {
    userList.token = await login();
  }
  next();
});

export default router;
