import { defineStore } from "pinia";

export const useRegionList = defineStore("regionList", {
  state: () => ({
    regionList: [],
  }),
});
