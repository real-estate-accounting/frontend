import { defineStore } from "pinia";

export const useCompanyList = defineStore("companyList", {
  state: () => ({
    companyList: [],
  }),
});
