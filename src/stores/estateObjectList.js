import { defineStore } from "pinia";

export const useEstateObjectList = defineStore("estateObjectList", {
  state: () => ({
    estateObjectList: [],
  }),
});
