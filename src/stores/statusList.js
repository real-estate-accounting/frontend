import { defineStore } from "pinia";

export const useStatusList = defineStore("statusList", {
  state: () => ({
    statusList: [],
  }),
});
