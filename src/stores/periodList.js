import { defineStore } from "pinia";

export const usePeriodList = defineStore("periodList", {
  state: () => ({
    periodList: [],
  }),
});
