import { defineStore } from "pinia";

export const useStateList = defineStore("stateList", {
  state: () => ({
    stateList: [],
  }),
});
