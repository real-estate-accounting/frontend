import { defineStore } from "pinia";

export const useUserList = defineStore("userList", {
  state: () => ({
    userList: [],
    token: null,
  }),
});
