export async function sendDataToServer(url, token, body) {
  const requestOptions = {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token.access}`,
    },
    body: JSON.stringify(body),
  };
  const response = await fetch(url, requestOptions);
  if (response.ok) {
    const json = await response.json();
    return json;
  } else {
    throw new Error(response.statusText);
  }
}
