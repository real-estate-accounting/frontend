import { defineStore } from "pinia";

export const useDistrictList = defineStore("districtList", {
  state: () => ({
    districtList: [],
  }),
});
