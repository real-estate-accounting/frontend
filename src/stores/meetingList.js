import { defineStore } from "pinia";

export const useMeetingList = defineStore("meetingList", {
  state: () => ({
    meetingList: [],
  }),
});
