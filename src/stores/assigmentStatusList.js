import { defineStore } from "pinia";

export const useAssigmentStatusList = defineStore("assigmentStatusList", {
  state: () => ({
    assigmentStatusList: [],
  }),
});
