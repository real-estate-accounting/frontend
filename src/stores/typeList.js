import { defineStore } from "pinia";

export const useTypeList = defineStore("typeList", {
  state: () => ({
    typeList: [],
  }),
});
