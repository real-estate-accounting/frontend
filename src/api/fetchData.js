export async function fetchData(url, token) {
  const requestOptions = {
    method: "GET",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token.access}`,
    },
  };
  const response = await fetch(url, requestOptions);
  if (response.ok) {
    const json = await response.json();
    return json;
  } else {
    throw new Error(response.statusText);
  }
}
