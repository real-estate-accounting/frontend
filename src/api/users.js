export async function login() {
  const requestOptions = {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username: "admin", password: "secret" }),
  };
  const responseUserList = await fetch(
    `${import.meta.env.VITE_API_URL}/token/`,
    requestOptions
  );
  if (responseUserList.ok) {
    const token = await responseUserList.json();
    return token;
  } else {
    throw new Error(responseUserList.statusText);
  }
}
export async function refresh(token) {
  const requestOptions = {
    method: "POST",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ refresh: token.refresh }),
  };
  const response = await fetch(
    `${import.meta.env.VITE_API_URL}/token/refresh/`,
    requestOptions
  );
  if (response.ok) {
    const json = await response.json();
    token.access = json.access;
    return token;
  } else {
    throw new Error(response.statusText);
  }
}
export async function verify(token) {
  if (token) {
    const requestOptions = {
      method: "POST",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ token: token.access }),
    };
    const response = await fetch(
      `${import.meta.env.VITE_API_URL}/token/verify/`,
      requestOptions
    );
    if (!response.ok === 401) {
      throw new Error(response.statusText);
    }
  }
}
